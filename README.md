<div align="center">

<img src="https://gitlab.com/arguser/croissant/raw/master/croissant.png" width="150" />

# Croissant

Who should bring Croissant to the office this Friday?

</div>


## Description

[Croissant](https://arguser.gitlab.io/croissant/) is the product of my first contact with Elm. In order to exercise I've decided to do a little application to tell who's in charge for croissants every Friday. I wanted it to be simple, no backend involved, so I've hardcoded an arbritary date from which I calculate the elapsed weeks to iterate over a list of team-mates.
