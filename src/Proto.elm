module Proto exposing (importLinks)

import Html exposing (Html, div, node)
import Html.Attributes exposing (class, href, rel)


linkURLs : List String
linkURLs =
    [ "https://unpkg.com/tachyons@4.9.0/css/tachyons.min.css"
    ]


importLinks : List (Html msg)
importLinks =
    List.map toStylesheet linkURLs


toStylesheet : String -> Html msg
toStylesheet x =
    Html.node "link" [ Html.Attributes.rel "stylesheet", Html.Attributes.href x ] []
