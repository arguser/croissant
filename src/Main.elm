module Main exposing (Model, Msg(..), colorList, getCurrentDateText, getCurrentMember, getCurrentMembers, getMembers, getTime, getWeeksFromStartDate, init, main, seed, subscriptions, update, view, viewMessage)

import Browser exposing (..)
import Date exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import List.Extra exposing (getAt)
import Proto exposing (..)
import Random exposing (int)
import Tachyons exposing (classes, tachyons)
import Tachyons.Classes exposing (..)
import Task exposing (..)
import Time exposing (..)


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- INITIALIZE


seed =
    Random.initialSeed 1


randomSeed =
    ( Random.step (Random.int 0 (List.length colorList)), seed )


init : Int -> ( Model, Cmd Msg )
init currentTime =
    ( Model "" getMembers False Nothing (Date.fromCalendarDate 2017 Dec 23) bg_white, Random.generate RandomBackgroundColor (Random.int 0 (List.length colorList)) )


getTime : Cmd Msg
getTime =
    Task.perform CheckDate Time.now



-- MODEL


type alias Model =
    { currentMember : String
    , members : List String
    , updatedCurrentMember : Bool
    , currentDate : Maybe Date.Date
    , startDate : Date.Date
    , backgroundColor : String
    }


type Msg
    = CheckDate Time.Posix
    | ChangeColor
    | NewBackgroundColor Int
    | RandomBackgroundColor Int


getMembers : List String
getMembers =
    [ "Joao", "Eugenia", "Jona", "Kevin", "Exe", "Leandro", "Nico", "Pablo", "Estefani", "Giuliana", "Lucas", "Seba" ]


colorList : List String
colorList =
    [ bg_blue, bg_yellow, bg_gold, bg_green, bg_pink, bg_purple, bg_red, bg_washed_blue, bg_washed_green, bg_washed_red, bg_washed_yellow, bg_dark_blue, bg_gray, bg_dark_green, bg_dark_pink, bg_orange, bg_light_blue, bg_light_green, bg_light_pink, bg_light_purple, bg_light_red ]



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        CheckDate newDate ->
            if Date.weekday (Date.fromPosix Time.utc newDate) == Time.Sat then
                if not model.updatedCurrentMember then
                    let
                        ( newCurrentMember, newMembersList ) =
                            getCurrentMember model (getWeeksFromStartDate model (Date.fromPosix Time.utc newDate))
                    in
                    ( { model | currentMember = newCurrentMember, members = newMembersList, updatedCurrentMember = True, currentDate = Just (Date.fromPosix Time.utc newDate) }, Cmd.none )

                else
                    ( model, Cmd.none )

            else
                let
                    ( newCurrentMember, newMembersList ) =
                        getCurrentMember model (getWeeksFromStartDate model (Date.fromPosix Time.utc newDate))
                in
                ( { model | currentMember = newCurrentMember, members = newMembersList, updatedCurrentMember = True, currentDate = Just (Date.fromPosix Time.utc newDate) }, Cmd.none )

        ChangeColor ->
            ( model, Random.generate NewBackgroundColor (Random.int 0 (List.length colorList)) )

        NewBackgroundColor index ->
            ( { model | backgroundColor = Maybe.withDefault bg_gold (getAt index colorList) }, Cmd.none )

        RandomBackgroundColor index ->
            ( { model | backgroundColor = Maybe.withDefault bg_gold (getAt index colorList) }, getTime )


getCurrentMembers : List String -> Int -> ( String, List String )
getCurrentMembers members int =
    if int == 0 then
        ( Maybe.withDefault "No hay miembros" (List.head members)
        , List.append
            (Maybe.withDefault [] (List.tail members))
            (List.take 1 members)
        )

    else
        getCurrentMembers
            (List.append
                (Maybe.withDefault [] (List.tail members))
                (List.take 1 members)
            )
            (int - 1)


getWeeksFromStartDate : Model -> Date.Date -> Int
getWeeksFromStartDate model date =
    Date.diff
        Date.Weeks
        model.startDate
        date



--getCurrentMember : Model -> Int -> (a -> ( String, List String ))


getCurrentMember model int =
    let
        iterations =
            remainderBy (List.length model.members) int
    in
    getCurrentMembers model.members iterations



-- VIEW


view : Model -> Html Msg
view model =
    div [ classes [ model.backgroundColor, Tachyons.Classes.code ] ]
        (Proto.importLinks
            ++ [ Html.header [ id "header", classes [ tc, pv4 ] ]
                    [ div [ classes [] ]
                        [ Html.time [ classes [ mt2, mb0, f6, fw5, dib, ttu, tracked ] ] [ Html.small [] [ text (getCurrentDateText model.currentDate) ] ]
                        , Html.h1 [ classes [ mt2, mb0, fw1, f1, Tachyons.Classes.garamond ] ] [ Html.span [ classes [ bg_black_80, lh_copy, white, pa1, tracked_tight ] ] [ text "Croissant!" ] ]
                        , Html.h2 [ classes [ mt2, mb0, f6, fw4, tracked ] ] [ Html.img [ src "croissant.svg", type_ "image/svg+xml", classes [ w5 ], onClick ChangeColor ] [] ]
                        ]
                    , Html.h2 [ classes [ f4, ttu, dib ] ] [ text model.currentMember ]
                    ]
               , div [ classes [ pa3, pa5_ns ] ] [ ul [ classes [ Tachyons.Classes.list, pl0, measure, center ] ] (List.map viewMessage (List.take (List.length model.members - 1) model.members)) ]
               , Html.footer [ classes [ pv4, ph3, ph5_m, ph6_l, db ] ]
                    [ Html.small [ classes [ f6, db, Tachyons.Classes.tc ] ]
                        [ text "2017"
                        , Html.b [ classes [ ttu ] ] [ text "tranquera" ]
                        ]
                    , Html.small
                        [ classes
                            [ db
                            , Tachyons.Classes.i
                            , Tachyons.Classes.tc
                            ]
                        ]
                        [ a [ href "http://elm-lang.org" ] [ text "Made with Elm" ] ]
                    ]
               ]
        )


viewMessage : String -> Html msg
viewMessage msg =
    li [ classes [ lh_copy, pv3, ba, bl_0, bt_0, br_0, b__dotted, b__black_30, Tachyons.Classes.tc, w5, center ] ] [ text msg ]


getCurrentDateText : Maybe Date.Date -> String
getCurrentDateText currentDate =
    Maybe.withDefault "" <| Maybe.map (format "dd/MM/yyyy") currentDate



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every (60 * 60 * 1000) CheckDate
