const captureWebsite = require('capture-website');

(async () => {
  await captureWebsite.file('https://arguser.gitlab.io/croissant/', 'public/croissant.png', {
    width: 300,
    height: 600,
    element: 'header',
    launchOptions: {
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox'
      ]
    }
  });
})();
